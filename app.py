import redis
import requests

from flask import Flask
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask_session import Session

from auth.auth import login_required
from recursos.produtos import produtos

app = Flask(__name__)
app.register_blueprint(produtos)
app.config["TEMPLATES_AUTO_RELOAD"] = True
app.config["EXPLAIN_TEMPLATE_LOADING"] = True
app.config["SECRET_KEY"] = "da539c3d-d20c-4064-9623-2a0bc05a708f"
app.config["SESSION_TYPE"] = "redis"
app.config["SESSION_REDIS"] = redis.from_url("redis://200.100.50.21:6379")

server_session = Session(app)


@app.route("/login")
def render_login():
    return render_template("views/login.html")


@app.route("/login", methods=["POST"])
def login():
    response = dict(request.form)
    if response["login"].lower() == "daniel" and response["password"] == "teste":
        session["auth"] = True
        return redirect("/")
    else:
        return redirect("/login")


@app.route("/sair")
def logout():
    session["auth"] = False
    return redirect("/")


@app.route("/profile")
@login_required
def get_profile():
    context = {
        "name": "Margareth Foster",
        "email": "margareth.foster@gmail.com",
        "role": "admin",
    }

    return render_template("views/profile.html", context=context)


@app.route("/")
@login_required
def get_teste():
    produtos = requests.get("http://200.100.50.2:5000/produtos")
    produtos = produtos.json()
    context = {
        "results": [
            {
                "name": "Usuários",
                "icon": "fas fa-user-circle fa-2x",
                "total": produtos.__len__(),
            },
            {
                "name": "Valor do Estoque",
                "icon": "fas fa-dollar-sign fa-2x",
                "total": 200352,
            },
            {"name": "Produtos", "icon": "fas fa-gift fa-2x", "total": 8282},
        ],
        "table": produtos,
    }

    return render_template("views/index.html", context=context)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=8080)
