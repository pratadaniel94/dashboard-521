import requests

from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request

from auth.auth import login_required

produtos = Blueprint("produtos", __name__, url_prefix="/produto")
ENDPOINT = "http://200.100.50.2:5000/produtos"


@produtos.route("")
@login_required
def get_products():
    data = requests.get(ENDPOINT)
    return render_template("views/product.html", context=data.json())


@produtos.route("/update/<id>", methods=["GET"])
@login_required
def get_update_product(id: str):
    data = requests.get(f"{ENDPOINT}/{id}")
    return render_template("views/edit-product.html", context=data.json())


@produtos.route("/update/<id>", methods=["POST"])
@login_required
def post_update_product(id: str):
    headers = {"Content-Type": "application/json"}
    aux = dict(request.form)
    aux["qtde"] = int(aux["qtde"])
    aux["price"] = float(aux["price"])
    response = requests.put(f"{ENDPOINT}/{id}", headers=headers, json=aux)
    if response.status_code == 200:
        return redirect("/produto")
    else:
        return redirect(f"/update/{id}")


@produtos.route("/delete/<id>")
@login_required
def delete_product(id: str):
    data = requests.delete(f"{ENDPOINT}/{id}")
    return redirect("/produto")


@produtos.route("/register")
@login_required
def get_register_product():
    return render_template("views/edit-product.html", context={"register": True})


@produtos.route("/register", methods=["POST"])
@login_required
def post_register_product():
    headers = {"Content-Type": "application/json"}
    aux = dict(request.form)
    aux["qtde"] = int(aux["qtde"])
    aux["price"] = float(aux["price"])
    response = requests.post(f"{ENDPOINT}", headers=headers, json=aux)
    if response.status_code == 201:
        return redirect("/produto")
    else:
        return redirect(f"/update/{id}")
