from functools import wraps

from flask import redirect
from flask import session


def login_required(fn):
    @wraps(fn)
    def decorated_function(*args, **kwargs):
        if not session.get("auth"):
            return redirect("/login")
        return fn(*args, **kwargs)

    return decorated_function
