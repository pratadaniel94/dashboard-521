# imagem base
FROM python:3.10-slim as base

# Cria pasta
RUN mkdir /packages

# Copia os arquivos de config do poetry
COPY pyproject.toml poetry.lock /packages/

# move para o diretorio
WORKDIR /packages


# atualiza pip instala poetry
RUN pip install --upgrade pip && \
    pip install poetry


# gera arquivo requerimentos e desistala poetry e instala dependencias
RUN poetry config virtualenvs.create false && poetry install
#RUN poetry export -f requirements.txt -o requirements.txt && \
#    pip uninstall --yes poetry && \
#    pip install --require-hashes --prefix=/packages -r requirements.txt

# Cria um novo estagio
FROM base

# Copia os binarios do pacote python para dentro da nova imagem
COPY --from=base /packages /usr/local/

# move para diretorio app
WORKDIR /app

# copia arquivos para dentro do container
COPY . /app

# expoe porta 8080
EXPOSE 8080

# executa aplicação
CMD ["python3", "app.py"]
